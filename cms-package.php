<?php include 'header.php'; ?>
<div class="hero-wrap" style="background-image: url('public/images/bg_1.jpg');height:200px!important;"></div>

<?php if(Session::isLogin() == 1): ?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <br>
            <h3>Create a new package</h3>
            <form action="actions/create-package.php" method="POST" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="">Package name</label>
                    <input name="name" type="text" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Package price</label>
                    <input name="price" type="text" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Package avatar</label>
                    <input name="photo" type="file" class="form-control" id="">
                </div>
                <div id="editor" style="min-height:300px;"></div>
                <textarea name="description" id="description" style="display:none;"></textarea>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <br>
        </div>
    </div>
</div>
<?php else: ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Page not found</h1>
        </div>
    </div>
</div>
<?php endif; ?>

<?php include 'footer.php' ?>