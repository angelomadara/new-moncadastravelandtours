<?php include 'header.php'; ?>
<?php 
    $package = (new Query())->select('packages')->where('id','=',Request::get('id'))->first(); 
?>

<div class="hero-wrap blur" style="background-image: url(<?= $package->photo; ?>);min-height:300px;position:relative;">
    <div class="overlay"></div>
    <div class="container">
    <div class="row slider-text centered" style="top:70%!important">
        <div class="">
            <!-- <h1 class="mb-3 bread"><?= htmlspecialchars_decode($package->name) ?></h1> -->
        </div>
    </div>
    </div>
</div>

<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
        <div class="col-lg-12">
				<div class="row">
					<?php if($package): ?>
						<div class="col-md-12 ftco-animate">
							<div class="destination">
								<div>
									<img src="<?= $package->photo; ?>" alt="" style="width:100%">
								</div>
								<!-- <a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= $package->photo; ?>);height:300px;">
									<div class="icon d-flex justify-content-center align-items-center">
										<span class="icon-search2"></span>
									</div>
								</a> -->
								<div class="text p-3">
									<div class="" style="text-align:center">
										<div class="">
											<h2><a href="#"><?= htmlspecialchars_decode($package->name) ?></a></h2>
										</div>
										<div class="" style="">
											<h3><span class="price"><?= $package->price ?></span></h3>
										</div>
									</div>
									<?= htmlspecialchars_decode($package->description) ?>
									<!-- <hr> -->
								</div>
							</div>
						</div>
                    <?php endif; ?>
				</div>
			</div> 
        </div>
    </div>
</div>

<?php include 'footer.php' ?>