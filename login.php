<?php
require_once 'init.php';
require_once 'includes/functions.php';
spl_autoload_register(function($class){
	require_once "app/".$class.".php";
});
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="public/css/util.css">
    <link rel="stylesheet" href="public/css/login.css">
</head>
<body>
	<div class="limiter">
        <div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
				<form action="actions/login.php" method='post' id="login-form" class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-51"> Login </span>
					<input type="hidden" name="_token" id="_token" value="<?= Token::generateToken() ?>">
					<div class="wrap-input100 validate-input m-b-16">
						<input type="email" name="email" class="input100" id="" aria-describedby="emailHelp" placeholder="Enter email" required>
					</div>
					<div class="wrap-input100 validate-input m-b-16">
						<input type="password" name="password" class="input100" id="" placeholder="Password" required>
					</div>
					<div class="container-login100-form-btn m-t-17">
					<button type="submit" class="login100-form-btn">Login</button>
					</div>
				</form>
			</div>
    	</div>
	</div>
</body>
</html>

