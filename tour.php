<?php include 'header.php'; ?>
<?php $packages = (new Query())->select('packages')->get(); ?>

<div class="hero-wrap js-fullheight" style="background-image: url('public/images/bg-3.jpg');">
    <div class="overlay"></div>
    <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-9 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="index.html">Home</a></span> <span>Tour</span></p>
        <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Destination</h1>
        </div>
    </div>
    </div>
</div>


<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
        <div class="col-lg-12">
				<div class="row">
					<?php foreach($packages as $package): ?>
						<div class="col-md-4 ftco-animate">
							<div class="destination">
								<a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= $package->photo; ?>);">
									<div class="icon d-flex justify-content-center align-items-center">
										<span class="icon-search2"></span>
									</div>
								</a>
								<div class="text p-3">
									<div class="d-flex">
										<div class="one">
											<h3><a href="#"><?= $package->name ?></a></h3>
											<p class="rate">
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<span>8 Rating</span>
											</p>
										</div>
										<div class="two">
											<span class="price"><?= $package->price ? "PHP ".$package->price : '' ?></span>
										</div>
									</div>
									<!-- <?= mb_strimwidth($package->description, 0, 200, "...") ?> -->
									<hr>
									<p class="bottom-area d-flex">
										<!-- <span><i class="icon-map-o"></i> Baguio</span>  -->
										<span class="ml-auto"><a href="package.php?id=<?=$package->id?>">Discover</a></span>
									</p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<!-- <div class="row mt-5">
					<div class="col text-center">
						<div class="block-27">
						<ul>
							<li><a href="#">&lt;</a></li>
							<li class="active"><span>1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">&gt;</a></li>
						</ul>
						</div>
					</div>
                </div> -->
			</div> <!-- .col-md-8 -->
        </div>
    </div>
</div>

<?php include 'footer.php' ?>