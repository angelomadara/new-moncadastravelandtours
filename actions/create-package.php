<?php
require_once '../init.php';
require_once '../includes/functions.php';
spl_autoload_register(function($class){
    require_once "../app/".$class.".php";
    // echo $class;
});
// echo Request::get('name',false);
// exit();
$msg = "?class=alert alert-danger&message=You are not logged-in";

if(Session::isLogin()==1){
    // print_r($_FILES['photo']);
    $dir_temp = "../tmp/";
    $dir_dis  = "../public/images/tours/";
    // filename 
    $photo_name = $_FILES['photo']['name'];
    $temp_name  = $_FILES['photo']['tmp_name'];
    $photo_type = $_FILES['photo']['type'];
    // only allowed extensions
    $only_extension_allowed = array("gif", "jpeg", "jpg", "png");
    // filtering extension name
    $explode_temp = explode(".", $photo_name);
    // identifying extension
    $extension = strtolower(end($explode_temp));
    // identifying the first name of the file
    $firstname = reset($explode_temp);

    // checking the extenstions
	if (in_array($extension, $only_extension_allowed)){
            // create new photo name
            $new_name = rand(0,1000).date('dmY').time().".".$extension;

            // migrating photo into temporary folder
            // move_uploaded_file($temp_name,$dir_temp.$photo_name);

            // resizing the photo
            // $resizeObj = new Image($dir_temp.$photo_name);
            // $resizeObj->resizeImage(1366, 900, 'landscape');
            // $resizeObj->saveImage($dir_dis.$new_name , 100);

            move_uploaded_file($temp_name,$dir_dis.$new_name);

            /**
             * create package
             */
            Query::create('packages',[
                'name'          => Request::get('name',false),
                'description'   => Request::get('description',false),
                'photo'         => 'public/images/tours/'.$new_name,
                'price'         => Request::get('price',false),
            ]);

            $msg = "?class=alert alert-success&message=New package created";
            
            // delete temporary photo
            // unlink($dir_temp.$photo_name);

    }else{
        $msg = "?class=alert alert-danger&message=Something went wrong please try again uploading your photo.";
    }

    
}

Redirect::to('../../cms-package.php'.$msg);